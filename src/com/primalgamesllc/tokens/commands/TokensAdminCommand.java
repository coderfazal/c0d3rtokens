package com.primalgamesllc.tokens.commands;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import com.primalgamesllc.tokens.database.SQLiteDatabase;
import com.primalgamesllc.tokens.utils.ItemUtils;
import com.primalgamesllc.tokens.utils.MessageUtils;
import com.primalgamesllc.tokens.utils.ShopUtils;
import com.primalgamesllc.tokens.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;

public class TokensAdminCommand implements CommandExecutor {

    public TokensAdminCommand() {
        System.out.print("[C0d3rTokens] Loaded TokensAdminCommand as /tokensadmin");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("tokensadmin")) {
            if (args.length == 0) {
                MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_HELP");
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("reload")) {
                    Long currentTime = System.currentTimeMillis();
                    Main.getInstance().setMobDrops(null);
                    Main.getInstance().setShopList(null);
                    Main.getInstance().reloadConfig();
                    Main.getInstance().setShopList(ShopUtils.getInstance().getShops());
                    Main.getInstance().setMobDrops(ItemUtils.getMobHeads());
                    MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_RELOAD", "%time%", String.valueOf(System.currentTimeMillis() - currentTime));
                } else if (args[0].equalsIgnoreCase("save")) {
                    Long currentTime = System.currentTimeMillis();
                    DataManager.getInstance().unregister();
                    Main.getInstance().getDataBase().shutdown();
                    Main.getInstance().setDatabase(null);
                    Main.getInstance().setDatabase((new SQLiteDatabase(new File(Main.getInstance().getDataFolder(), "/database/tokenDatabase.db"))));
                    Main.getInstance().getDataBase().open();
                    Main.getInstance().getDataBase().setup();
                    DataManager.getInstance().register();
                    MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_SAVE", "%time%", String.valueOf(System.currentTimeMillis() - currentTime));
                } else MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_USAGE");
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("give")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                    } else {
                        DataPlayer dataTarget = DataManager.getInstance().getByPlayer(target);
                        if (dataTarget == null) {
                            MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                            System.out.print("[C0d3rTokens] Failed to search for " + args[1] + " in the database.");
                        } else {
                            if (Utils.isInteger(args[2])) {
                                int amount = Integer.parseInt(args[2]);
                                if (amount <= 0) {
                                    MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                                } else {
                                    dataTarget.giveTokens(amount);
                                    MessageUtils.getInstance().sendMessageList(target, "TOKENS_ADMIN_BALANCE_ADJUST");
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        }
                    }
                } else if (args[0].equalsIgnoreCase("remove")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                    } else {
                        DataPlayer dataTarget = DataManager.getInstance().getByPlayer(target);
                        if (dataTarget == null) {
                            MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                            System.out.print("[C0d3rTokens] Failed to search for " + args[1] + " in the database.");
                        } else {
                            if (Utils.isInteger(args[2])) {
                                int amount = Integer.parseInt(args[2]);
                                if (amount <= 0) {
                                    MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                                } else {
                                    dataTarget.removeTokens(amount);
                                    MessageUtils.getInstance().sendMessageList(target, "TOKENS_ADMIN_BALANCE_ADJUST");
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        }
                    }
                } else if (args[0].equalsIgnoreCase("set")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    if (target == null) {
                        MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                    } else {
                        DataPlayer dataTarget = DataManager.getInstance().getByPlayer(target);
                        if (dataTarget == null) {
                            MessageUtils.getInstance().sendMessageList(sender, "PLAYER_NOT_FOUND");
                            System.out.print("[C0d3rTokens] Failed to search for " + args[1] + " in the database.");
                        } else {
                            if (Utils.isInteger(args[2])) {
                                int amount = Integer.parseInt(args[2]);
                                if (amount <= 0) {
                                    MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                                } else {
                                    dataTarget.setBalance(amount);
                                    MessageUtils.getInstance().sendMessageList(target, "TOKENS_ADMIN_BALANCE_ADJUST");
                                }
                            } else MessageUtils.getInstance().sendMessageList(sender, "INVALID_INTEGER");
                        }
                    }
                } else MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_USAGE");
            } else MessageUtils.getInstance().sendMessageList(sender, "TOKENS_ADMIN_USAGE");
        }
        return false;
    }
}
