package com.primalgamesllc.tokens.commands;

import com.primalgamesllc.tokens.inventory.models.MainMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TransferCommand implements CommandExecutor {

    public TransferCommand() {
        System.out.print("[C0d3rTokens] Loaded TransferCommand as /transfer");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("transfer")) {
            if (commandSender instanceof Player) {
                MainMenu.openMainMenu((Player) commandSender);
            } else commandSender.sendMessage("§4Only players are allowed to execute this command.");
        }
        return false;
    }
}
