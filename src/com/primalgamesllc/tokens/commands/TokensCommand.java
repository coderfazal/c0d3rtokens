package com.primalgamesllc.tokens.commands;

import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import com.primalgamesllc.tokens.utils.MessageUtils;
import com.primalgamesllc.tokens.utils.SoundUtils;
import com.primalgamesllc.tokens.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;

public class TokensCommand implements CommandExecutor {

    public TokensCommand() {
        System.out.print("[C0d3rTokens] Loaded TokensCommand as /tokens");
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("tokens")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
                if (dataPlayer == null) {
                    System.out.print("[C0d3rTokens] The player tried to accessed a command but was denied because the player didn't exist in the database.");
                    return false;
                }
                if (args.length == 0) {
                    MessageUtils.getInstance().sendMessageList(player, "TOKEN_BALANCE", "%balance%", NumberFormat.getInstance().format(dataPlayer.getBalance()));
                    new SoundUtils(player).playSound("TOKEN_BALANCE");
                } else {
                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("help")) {
                            MessageUtils.getInstance().sendMessageList(player, "TOKENS_HELP");
                        } else MessageUtils.getInstance().sendMessageList(player, "TOKENS_USAGE");
                    } else {
                        if (args.length == 3) {
                            if (args[0].equalsIgnoreCase("pay")) {
                                if (args[1].equalsIgnoreCase(player.getName())) {
                                    MessageUtils.getInstance().sendMessageList(player, "TOKENS_PAY_YOURSELF");
                                    return false;
                                }
                                Player target = Bukkit.getPlayer(args[1]);
                                if (target == null) {
                                    MessageUtils.getInstance().sendMessageList(player, "PLAYER_NOT_FOUND");
                                } else {
                                    DataPlayer dataTarget = DataManager.getInstance().getByPlayer(target);
                                    if (dataTarget == null) {
                                        MessageUtils.getInstance().sendMessageList(player, "PLAYER_NOT_FOUND");
                                        System.out.print("[C0d3rTokens] Failed to search for " + args[1] + " in the database.");
                                    } else {
                                        if (Utils.isInteger(args[2])) {
                                            int amount = Integer.parseInt(args[2]);
                                            if (amount <= 0) {
                                                MessageUtils.getInstance().sendMessageList(player, "INVALID_INTEGER");
                                            } else {
                                                if (amount > dataPlayer.getBalance()) {
                                                    MessageUtils.getInstance().sendMessageList(player, "NOT_ENOUGH_TOKENS", "%amount%", NumberFormat.getInstance().format(amount));
                                                    return false;
                                                }
                                                dataPlayer.removeTokens(amount);
                                                dataTarget.giveTokens(amount);
                                                MessageUtils.getInstance().sendMessageList(player, "TOKENS_PAID", "%player%", target.getName());
                                                MessageUtils.getInstance().sendMessageList(target, "TOKENS_RECEIVED", "%player%", player.getName());
                                            }
                                        } else MessageUtils.getInstance().sendMessageList(player, "INVALID_INTEGER");
                                    }
                                }
                            } else MessageUtils.getInstance().sendMessageList(player, "TOKENS_USAGE");
                        } else MessageUtils.getInstance().sendMessageList(player, "TOKENS_USAGE");
                    }
                }
            } else sender.sendMessage("§4Only players are allowed to execute this command.");
        }
        return false;
    }


}
