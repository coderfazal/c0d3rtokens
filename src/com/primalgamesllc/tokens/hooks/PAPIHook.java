package com.primalgamesllc.tokens.hooks;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.entity.Player;

import java.text.NumberFormat;

public class PAPIHook extends EZPlaceholderHook {

    private Main plugin;

    public PAPIHook(Main plugin) {
        super(plugin, "c0d3rtokens");
        this.plugin = plugin;
    }

    @Override
    public String onPlaceholderRequest(Player player, String subplaceholder) {
        if (subplaceholder.equalsIgnoreCase("balance")) {
            DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
            if (dataPlayer == null) {
                return "&cThe player was not found in the database.";
            }
            if (dataPlayer.getBalance() > 0) {
                return NumberFormat.getInstance().format(dataPlayer.getBalance());
            }
            return String.valueOf(0);
        }
        return "&cThe placeholder was not found.";
    }

}
