package com.primalgamesllc.tokens.hooks;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.api.events.MobDropEvent;
import me.swanis.boosters.BoostersAPI;
import me.swanis.boosters.booster.Booster;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

import java.util.List;

import static me.swanis.boosters.BoostersAPI.getBoosterManager;

public class SuperBoosterHook implements Booster {


    @Override
    public String getName() {
        return "Tokens";
    }

    @Override
    public String getCommand() {
        return "";
    }

    @Override
    public String getResetCommand() {
        return "";
    }

    @Override
    public Material getMaterial() {
        return Material.getMaterial(Main.getInstance().getConfig().getString("booster.material"));
    }

    @Override
    public short getDurability() {
        return (short)Main.getInstance().getConfig().getInt("booster.data");
    }

    @Override
    public boolean isGlow() {
        return Main.getInstance().getConfig().getBoolean("booster.enchanted");
    }

    @Override
    public String getItemName() {
        return Main.getInstance().getConfig().getString("booster.name");
    }

    @Override
    public List<String> getActivateLore() {
        return Main.getInstance().getConfig().getStringList("booster.lore.activate");
    }

    @Override
    public List<String> getBoostersLore() {
        return Main.getInstance().getConfig().getStringList("booster.lore.activated");
    }

    @Override
    public int getSlot() {
        return Main.getInstance().getConfig().getInt("booster.slot");
    }

    @Override
    public void reload() {
        return;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMobDrop(MobDropEvent e){
        if (getBoosterManager().getActiveBooster(this) == null) {
            return;
        }
        e.setCancelled(true);
        Location location = e.getLocation();
        ItemStack itemStack = e.getItemStack();
        itemStack.setAmount(itemStack.getAmount() * 2);
        location.getWorld().dropItemNaturally(location, itemStack);
    }

}
