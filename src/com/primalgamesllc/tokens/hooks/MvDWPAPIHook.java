package com.primalgamesllc.tokens.hooks;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;

import java.text.NumberFormat;

public class MvDWPAPIHook {

    private static MvDWPAPIHook instance;

    public static MvDWPAPIHook getInstance() {
        if (MvDWPAPIHook.instance == null) {
            synchronized (MvDWPAPIHook.class) {
                if (MvDWPAPIHook.instance == null) {
                    MvDWPAPIHook.instance = new MvDWPAPIHook();
                }
            }
        }
        return MvDWPAPIHook.instance;
    }

    public void hook() {
        PlaceholderAPI.registerPlaceholder(Main.getInstance(), "c0d3rtokens_balance",
                new PlaceholderReplacer() {
                    @Override
                    public String onPlaceholderReplace(PlaceholderReplaceEvent e) {
                        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(e.getPlayer());
                        if (dataPlayer == null) {
                            return "&cThe player was not found in the database.";
                        }
                        if (dataPlayer.getBalance() > 0) {
                            return NumberFormat.getInstance().format(dataPlayer.getBalance());
                        }
                        return String.valueOf(0);
                    }
                });
    }
}