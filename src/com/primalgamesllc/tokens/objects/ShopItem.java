package com.primalgamesllc.tokens.objects;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.utils.EnchantGlow;
import com.primalgamesllc.tokens.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class ShopItem {

    private Integer slot;
    private Integer cost;

    private String itemKey;
    private String shopKey;

    private List<String> commandsList;

    private ItemStack displayItem;

    public ShopItem(String shopKey, String itemKey) {
        this.itemKey = itemKey;
        this.shopKey = shopKey;
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("shops." + shopKey + ".items." + itemKey);
        this.slot = section.getInt("displayItem.slot");
        this.cost = section.getInt("cost");
        this.commandsList = section.getStringList("commands");
        String material = section.getString("displayItem.material");
        int data = section.getInt("displayItem.data");
        String name = section.getString("displayItem.name");
        List<String> lore = section.getStringList("displayItem.lore");
        this.displayItem = new ItemBuilder(Material.getMaterial(material), data).setName(name).setLore(lore).getStack();
        if (section.getBoolean("displayItem.enchanted")) {
            EnchantGlow.addGlow(displayItem);
        }
    }

    public String getShopKey() {
        return shopKey;
    }

    public void setShopKey(String shopKey) {
        this.shopKey = shopKey;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public List<String> getCommandsList() {
        return commandsList;
    }

    public void setCommandsList(List<String> commandsList) {
        this.commandsList = commandsList;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public ItemStack getDisplayItem() {
        return displayItem;
    }

    public void setDisplayItem(ItemStack displayItem) {
        this.displayItem = displayItem;
    }

}
