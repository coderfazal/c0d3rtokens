package com.primalgamesllc.tokens.objects;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.utils.EnchantGlow;
import com.primalgamesllc.tokens.utils.ItemBuilder;
import com.primalgamesllc.tokens.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Shop {

    private Integer slot;
    private Integer size;

    private List<Integer> updateSlots;

    private Map<Integer, ShopItem> itemList;
    private Map<Integer, ShopItem> updateItemList;

    private ItemStack displayItem;

    private String title;
    private String updateTime;
    private String timeZone;
    private String shopKey;

    private boolean autoUpdate;

    public Shop(String shopKey) {
        this.shopKey = shopKey;
        ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("shops." + shopKey);
        this.slot = section.getInt("displayItem.slot");
        String material = section.getString("displayItem.material");
        int data = section.getInt("displayItem.data");
        String name = section.getString("displayItem.name");
        List<String> lore = section.getStringList("displayItem.lore");
        this.displayItem = new ItemBuilder(Material.getMaterial(material), data).setName(name).setLore(lore).getStack();
        this.size = section.getInt("size");
        this.title = section.getString("title");
        this.updateItemList = new HashMap<>();
        this.itemList = new HashMap<Integer, ShopItem>();
        this.autoUpdate = section.getBoolean("settings.autoUpdate.enabled");
        this.updateSlots = section.getIntegerList("settings.autoUpdate.itemSlots");
        this.updateTime = section.getString("settings.autoUpdate.updateTime");
        this.timeZone = section.getString("settings.autoUpdate.timeZone");
        for (String itemKey : section.getConfigurationSection("items").getKeys(false)) {
            ShopItem shopItem = new ShopItem(shopKey, itemKey);
            this.itemList.put(shopItem.getSlot(), shopItem);
        }
        if (section.getBoolean("displayItem.enchanted")) {
            EnchantGlow.addGlow(displayItem);
        }
        if (autoUpdate) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    updateShop();
                }
            }, ticksToUpdate());
            if (Main.getInstance().getData().getConfigurationSection("data." + this.shopKey) == null) {
                if (this.itemList.size() <= this.updateSlots.size()) {
                    for (int i = 0; i < this.itemList.size(); i++) {
                        this.updateItemList.put(this.updateSlots.get(i), this.itemList.get(i));
                    }
                } else {
                    List<ShopItem> shopItemList = new ArrayList<>();
                    for (ShopItem item : this.itemList.values()) {
                        shopItemList.add(item);
                    }
                    Random random = new Random();
                    for (int slot : this.updateSlots) {
                        if (shopItemList == null || shopItemList.isEmpty()) break;
                        ShopItem item = shopItemList.get(random.nextInt(shopItemList.size()));
                        shopItemList.remove(item);
                        this.updateItemList.put(slot, item);
                    }
                }
                for (int slot : this.updateItemList.keySet()) {
                    ShopItem item = this.updateItemList.get(slot);
                    Main.getInstance().getData().set("data." + shopKey + "." + item.getItemKey() + ".slot", slot);
                }
                Main.getInstance().saveData();
                Main.getInstance().reloadData();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    MessageUtils.getInstance().sendMessageList(player, "SHOP_AUTO_UPDATE_BROADCAST", "%shop%", shopKey);
                }
            } else {
                for (String itemKey : Main.getInstance().getData().getConfigurationSection("data." + this.shopKey).getKeys(false)) {
                    for (ShopItem item : this.itemList.values()) {
                        if (item.getItemKey().equalsIgnoreCase(itemKey)) {
                            this.updateItemList.put(Main.getInstance().getData().getInt("data." + this.shopKey + "." + itemKey + ".slot"), item);
                        }
                    }
                }
            }
        }

    }

    public void updateShop() {
        Main.getInstance().getData().set("data." + this.shopKey, null);
        this.updateItemList.clear();
        if (this.itemList.size() < this.updateSlots.size()) {
            for (int i = 0; i < this.itemList.size(); i++) {
                this.updateItemList.put(this.updateSlots.get(i), this.itemList.get(i));
            }
        } else {
            List<ShopItem> shopItemList = new ArrayList<>();
            for (ShopItem item : this.itemList.values()) {
                shopItemList.add(item);
            }
            Random random = new Random();
            for (int i = 0; i < this.updateSlots.size(); i++) {
                if (shopItemList == null || shopItemList.isEmpty()) break;
                ShopItem item = shopItemList.get(random.nextInt(shopItemList.size()));
                shopItemList.remove(item);
                this.updateItemList.put(this.updateSlots.get(i), item);
            }
        }
        for (int slot : this.updateItemList.keySet()) {
            ShopItem item = this.updateItemList.get(slot);
            Main.getInstance().getData().set("data." + shopKey + "." + item.getItemKey() + ".slot", slot);
        }
        Main.getInstance().saveData();
        Main.getInstance().reloadData();
        for (Player player : Bukkit.getOnlinePlayers()) {
            MessageUtils.getInstance().sendMessageList(player, "SHOP_AUTO_UPDATE_BROADCAST", "%shop%", shopKey);
        }
    }

    public Map<Integer, ShopItem> getUpdateItemList() {
        return updateItemList;
    }

    public void setUpdateItemList(Map<Integer, ShopItem> updateItemList) {
        this.updateItemList = updateItemList;
    }

    public Long ticksToUpdate() {
        long ms = updateTimeInMS();
        long msLeft = ms - System.currentTimeMillis();
        long ticks = msLeft / 50;
        return ticks;
    }

    public Long updateTimeInMS() {
        String[] timeHM = this.updateTime.trim().split(":");
        int hour = Integer.parseInt(timeHM[0]);
        int minute = Integer.parseInt(timeHM[1]);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(this.timeZone));
        if (calendar.get(11) == hour && calendar.get(12) >= minute) {
            calendar.add(5, 1);
        } else if (calendar.get(11) > hour) {
            calendar.add(5, 1);
        }
        calendar.set(11, hour);
        calendar.set(12, minute);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar.getTimeInMillis();
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    public List<Integer> getUpdateSlots() {
        return updateSlots;
    }

    public void setUpdateSlots(List<Integer> updateSlots) {
        this.updateSlots = updateSlots;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public ItemStack getDisplayItem() {
        return displayItem;
    }

    public void setDisplayItem(ItemStack displayItem) {
        this.displayItem = displayItem;
    }

    public Map<Integer, ShopItem> getItemList() {
        return itemList;
    }

    public void setItemList(Map<Integer, ShopItem> itemList) {
        this.itemList = itemList;
    }
}
