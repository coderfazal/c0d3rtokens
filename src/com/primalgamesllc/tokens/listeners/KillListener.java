package com.primalgamesllc.tokens.listeners;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.api.events.MobDropEvent;
import com.primalgamesllc.tokens.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class KillListener implements Listener {

    public KillListener() {
        System.out.print("[C0d3rTokens] Registered KillListener");
    }

    @EventHandler
    public void onKill(EntityDeathEvent event) {
        if (Main.getInstance().getMobDrops().containsKey(event.getEntity().getType().toString())) {
            ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("mobDrops." + event.getEntity().getType().toString());
            int chance = section.getInt("chance");
            if (RandomUtils.getRandomInteger(0, 100) < chance) {
                MobDropEvent mobDropEvent = new MobDropEvent(event.getEntity().getLocation(), Main.getInstance().getMobDrops().get(event.getEntity().getType().toString()));
                Bukkit.getPluginManager().callEvent(mobDropEvent);
            }
        }
    }

}
