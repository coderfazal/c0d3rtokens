package com.primalgamesllc.tokens.listeners;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.api.events.TokenGainEvent;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import com.primalgamesllc.tokens.utils.MessageUtils;
import com.primalgamesllc.tokens.utils.SoundUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {

    public InteractListener() {
        System.out.print("[C0d3rTokens] Registered InteractListener");
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction().toString().contains("RIGHT")) {
            ItemStack item = e.getItem();
            if (item == null) return;
            if (!item.hasItemMeta()) return;
            if (!item.getItemMeta().hasDisplayName()) return;
            int itemAmount = e.getItem().getAmount();
            Boolean found = false;
            for (ItemStack itemStack : Main.getInstance().getMobDrops().values()) {
                if (itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(item.getItemMeta().getDisplayName())) {
                    found = true;
                    break;
                }
            }
            if (found) {
                e.setCancelled(true);
                DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(e.getPlayer());
                if (dataPlayer != null) {
                    e.getPlayer().setItemInHand(null);
                    e.getPlayer().updateInventory();
                    MessageUtils.getInstance().sendMessageList(e.getPlayer(), "TOKEN_CLAIM", "%tokens%", String.valueOf(itemAmount));
                    SoundUtils soundUtils = new SoundUtils(e.getPlayer());
                    soundUtils.playSound("TOKEN_CLAIM");
                    TokenGainEvent tokenGainEvent = new TokenGainEvent(e.getPlayer(), dataPlayer.getBalance(), dataPlayer.getBalance() + itemAmount);
                    dataPlayer.giveTokens(itemAmount);
                    Bukkit.getServer().getPluginManager().callEvent(tokenGainEvent);
                }
            }
        }
    }

}
