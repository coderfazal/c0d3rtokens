package com.primalgamesllc.tokens.listeners;

import com.primalgamesllc.tokens.api.events.MobDropEvent;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class MobDropListener implements Listener {

    @EventHandler(priority =  EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMobDrop(MobDropEvent e){
        Location location = e.getLocation();
        ItemStack itemStack = e.getItemStack();
        location.getWorld().dropItemNaturally(location, itemStack);
    }

}
