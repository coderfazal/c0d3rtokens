package com.primalgamesllc.tokens.listeners;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.update.UpdateCheck;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    public JoinListener() {
        System.out.print("[C0d3rTokens] Registered JoinListener");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (!Main.getInstance().getConfig().getBoolean("updateCheck")) return;
        Player player = e.getPlayer();
        if (player.isOp() || player.hasPermission("c0d3rtokens.admin")) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    UpdateCheck updateCheck = new UpdateCheck("https://www.primalgamesllc.com/plugins/versions/primaltokens.txt", Main.getInstance().getDescription().getVersion());
                    if (!updateCheck.isUpdated()) {
                        player.sendMessage("§2§l[C0d3rTokens] §aYou are using an outdated version of C0d3rTokens!");
                        player.sendMessage("§2§l[C0d3rTokens] §aCurrent Version:§f " + Main.getInstance().getDescription().getVersion());
                        player.sendMessage("§2§l[C0d3rTokens] §aNew Version:§f " + updateCheck.getVersionFromURL());
                    } else {
                        player.sendMessage("§2§l[C0d3rTokens] §aYou are using the latest version of C0d3rTokens!");
                    }
                }
            }, 40L);
        }
    }

}
