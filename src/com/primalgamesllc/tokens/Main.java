package com.primalgamesllc.tokens;

import com.primalgamesllc.tokens.commands.TokensAdminCommand;
import com.primalgamesllc.tokens.commands.TokensCommand;
import com.primalgamesllc.tokens.commands.TransferCommand;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.database.Database;
import com.primalgamesllc.tokens.database.SQLiteDatabase;
import com.primalgamesllc.tokens.hooks.MvDWPAPIHook;
import com.primalgamesllc.tokens.hooks.PAPIHook;
import com.primalgamesllc.tokens.hooks.SuperBoosterHook;
import com.primalgamesllc.tokens.license.LicenseValidation;
import com.primalgamesllc.tokens.license.security.Security;
import com.primalgamesllc.tokens.listeners.InteractListener;
import com.primalgamesllc.tokens.listeners.JoinListener;
import com.primalgamesllc.tokens.listeners.KillListener;
import com.primalgamesllc.tokens.listeners.MobDropListener;
import com.primalgamesllc.tokens.menus.MenuAPI;
import com.primalgamesllc.tokens.objects.Shop;
import com.primalgamesllc.tokens.update.UpdateCheck;
import com.primalgamesllc.tokens.utils.ItemUtils;
import com.primalgamesllc.tokens.utils.ShopUtils;
import me.swanis.boosters.BoostersAPI;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Main extends JavaPlugin {

    private static Main instance;

    private Database database;
    private Map<Integer, Shop> shopList;
    private Map<String, ItemStack> mobDrops;
    private File dataf;
    private FileConfiguration data;
    private boolean booster;

    public static Main getInstance() {
        return instance;
    }

    public void onEnable() {
        saveDefaultConfig();
        setupData();
        instance = this;
        Integer configVersion = 1;
        if (!getConfig().isInt("config-version") || getConfig().getInt("config-version", 0) < configVersion) {
            this.getConfig().set("config-version", configVersion);
            this.getConfig().options().copyDefaults(true);
            this.saveConfig();
            System.out.print("[C0d3rTokens] Config has been reset because the config-versions were a mismatch to the current plugin version.");
        }
        (this.database = new SQLiteDatabase(new File(getDataFolder(), "/database/tokenDatabase.db"))).open();
        this.database.setup();
        DataManager.getInstance().register();
        Security security = new Security();
        loadListeners(new MobDropListener(), DataManager.getInstance(), security, new JoinListener(), new KillListener(), MenuAPI.getInstance(), new InteractListener());
        if (getConfig().getString("license") == null || getConfig().getString("license").equalsIgnoreCase("XXXX-XXXX-XXXX-XXXX")) {
            getServer().getPluginManager().disablePlugin(this);
            System.out.print("[C0d3rTokens] \"license\" option in config is either null or default.");
            System.out.print("[C0d3rTokens] Therefore the plugin will be disabled.");
        } else {
            if (!new LicenseValidation(getConfig().getString("license"), "https://www.primalgamesllc.com/license/verify.php", this).register())
                return;
        }
        getCommand("tokens").setExecutor(new TokensCommand());
        getCommand("transfer").setExecutor(new TransferCommand());
        getCommand("tokensadmin").setExecutor(new TokensAdminCommand());
        hookPlaceholders();
        hookBoosters();
        if (getConfig().getBoolean("updateCheck")) {
            UpdateCheck updateCheck = new UpdateCheck("https://www.primalgamesllc.com/plugins/versions/primaltokens.txt", getDescription().getVersion());
            updateCheck.checkUpdate();
        }
        this.shopList = ShopUtils.getInstance().getShops();
        this.mobDrops = ItemUtils.getMobHeads();
        System.out.print("C0d3rTokens has been enabled");
        security.init();
    }

    public void onDisable() {
        getServer().getScheduler().cancelTasks(this);
        DataManager.getInstance().unregister();
        this.database.shutdown();
        this.database = null;
        System.out.print("C0d3rTokens has been disabled");
    }

    private void setupData() {
        dataf = new File(getDataFolder(), "data.yml");
        if (!dataf.exists()) {
            dataf.getParentFile().mkdirs();
            saveResource("data.yml", false);
        }
        data = new YamlConfiguration();
        try {
            data.load(dataf);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void hookBoosters(){
        if(Bukkit.getPluginManager().getPlugin("SuperBoosters") != null){
            BoostersAPI.getBoosterManager().register(new SuperBoosterHook());
        }
    }

    public FileConfiguration getData() {
        return data;
    }

    public YamlConfiguration reloadData() {
        return YamlConfiguration.loadConfiguration(dataf);
    }

    public void saveData() {
        if (data == null || dataf == null) return;
        try {
            data.save(dataf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadListeners(Listener... listeners) {
        for (Listener listener : listeners) {
            getServer().getPluginManager().registerEvents(listener, this);
        }
    }

    private void hookPlaceholders() {
        System.out.print("[C0d3rTokens] Checking if PlaceholderAPI is available.");
        if (getServer().getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            System.out.print("[C0d3rTokens] PlaceholderAPI found! registering placeholders!");
            new PAPIHook(this).hook();
            System.out.print("[C0d3rTokens] (PlaceholderAPI) Registered placeholder %c0d3rtokens_balance%");
        } else {
            System.out.print("[C0d3rTokens] PlaceholderAPI not found placeholders will not be registered!");
        }
        System.out.print("[C0d3rTokens] Checking if MvDWPlaceholderAPI is available.");
        if (getServer().getPluginManager().isPluginEnabled("MvDWPlaceholderAPI")) {
            System.out.print("[C0d3rTokens] MvDWPlaceholderAPI found! registering placeholders!");
            MvDWPAPIHook.getInstance().hook();
            System.out.print("[C0d3rTokens] (MvDWPlaceholderAPI) Registered placeholder {c0d3rtokens_balance}");
        } else {
            System.out.print("[C0d3rTokens] PlaceholderAPI not found placeholders will not be registered.");
        }
    }

    public Map<Integer, Shop> getShopList() {
        return shopList;
    }

    public void setShopList(Map<Integer, Shop> shopList) {
        this.shopList = shopList;
    }

    public Map<String, ItemStack> getMobDrops() {
        return mobDrops;
    }

    public void setMobDrops(Map<String, ItemStack> mobDrops) {
        this.mobDrops = mobDrops;
    }

    public Database getDataBase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
//Copyright 2018 PrimalGames, LLC