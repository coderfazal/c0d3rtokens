package com.primalgamesllc.tokens.menus;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class MenuItem {
    private Menu menu;
    private int slot;
    private ItemStack itemStack;

    protected void addToMenu(final Menu menu) {
        this.menu = menu;
    }

    protected void removeFromMenu(final Menu menu) {
        if (this.menu == menu) {
            this.menu = null;
        }
    }

    public Menu getMenu() {
        return this.menu;
    }

    public int getSlot() {
        return this.slot;
    }

    public void setSlot(final int slot) {
        this.slot = slot;
    }

    public abstract void onClick(final Player p0, final InventoryClickType p1);

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack item, boolean update) {
        this.itemStack = item;
        if (update && this.getMenu() != null) {
            this.getMenu().addMenuItem(this, getSlot());
            this.getMenu().updateMenu();
        }
    }

    public abstract static class UnclickableMenuItem extends MenuItem {
        @Override
        public void onClick(final Player player, final InventoryClickType clickType) {
        }
    }
}
