package com.primalgamesllc.tokens.inventory.models;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.menus.InventoryClickType;
import com.primalgamesllc.tokens.menus.Menu;
import com.primalgamesllc.tokens.menus.MenuAPI;
import com.primalgamesllc.tokens.menus.MenuItem;
import com.primalgamesllc.tokens.objects.Shop;
import com.primalgamesllc.tokens.utils.ItemUtils;
import com.primalgamesllc.tokens.utils.SoundUtils;
import com.primalgamesllc.tokens.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MainMenu {

    public static void openMainMenu(Player player) {
        Menu menu = MenuAPI.getInstance().createMenu(Utils.toColor(Main.getInstance().getConfig().getString("menu.mainMenu.title")), Main.getInstance().getConfig().getInt("menu.mainMenu.size") / 9);
        SoundUtils soundUtils = new SoundUtils(player);
        for (Shop shop : Main.getInstance().getShopList().values()) {
            MenuItem icon = new MenuItem() {
                @Override
                public void onClick(Player player, InventoryClickType clickType) {
                    menu.closeMenu(player);
                    CategoryMenu.openCategoryMenu(shop, player, menu);
                }

            };
            icon.setItemStack(shop.getDisplayItem(), true);
            menu.addMenuItem(icon, shop.getSlot());
        }
        menu.setMenuCloseBehaviour(new MenuAPI.MenuCloseBehaviour() {
            @Override
            public void onClose(Player player, Menu menu, boolean bypass) {
                if (!bypass) {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            soundUtils.playSound("CLOSE_MAIN_MENU");
                        }
                    }, 5L);
                }
            }
        });
        for (int free = 0; free < menu.getInventory().getSize(); ++free) {
            if (menu.getInventory().getItem(free) == null) {
                final MenuItem.UnclickableMenuItem item = (MenuItem.UnclickableMenuItem) new MenuItem.UnclickableMenuItem() {
                    @Override
                    public ItemStack getItemStack() {
                        return ItemUtils.getUnusedItem();
                    }
                };
                menu.addMenuItem(item, free);
            }
        }
        soundUtils.playSound("OPEN_MAIN_MENU");
        menu.openMenu(player);
        menu.scheduleUpdateTask(player, 20);
    }

}
