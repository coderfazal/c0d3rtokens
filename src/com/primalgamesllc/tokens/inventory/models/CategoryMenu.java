package com.primalgamesllc.tokens.inventory.models;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.api.events.PurchaseEvent;
import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import com.primalgamesllc.tokens.menus.InventoryClickType;
import com.primalgamesllc.tokens.menus.Menu;
import com.primalgamesllc.tokens.menus.MenuAPI;
import com.primalgamesllc.tokens.menus.MenuItem;
import com.primalgamesllc.tokens.objects.Shop;
import com.primalgamesllc.tokens.objects.ShopItem;
import com.primalgamesllc.tokens.utils.ItemUtils;
import com.primalgamesllc.tokens.utils.MessageUtils;
import com.primalgamesllc.tokens.utils.SoundUtils;
import com.primalgamesllc.tokens.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class CategoryMenu {

    public static void openCategoryMenu(Shop shop, Player player, Menu parentMenu) {
        Menu menu = MenuAPI.getInstance().createMenu(Utils.toColor(shop.getTitle()), shop.getSize() / 9);
        SoundUtils soundUtils = new SoundUtils(player);
        menu.setParent(parentMenu);
        if (!shop.isAutoUpdate()) {
            for (ShopItem shopItem : shop.getItemList().values()) {
                MenuItem menuItem = new MenuItem() {
                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);

                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        if (dataPlayer == null) return;
                        if (dataPlayer.getBalance() >= shopItem.getCost()) {
                            menu.setBypassMenuCloseBehaviour(true);
                            dataPlayer.removeTokens(shopItem.getCost());
                            for (String command : shopItem.getCommandsList()) {
                                command = command.replaceAll("%player%", player.getName());
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                            }
                            menu.closeMenu(player);
                            soundUtils.playSound("SUCCESSFUL_PURCHASE");
                            MessageUtils.getInstance().sendMessageList(player, "PURCHASE_COMPLETE", "%item%", shopItem.getItemKey());
                            PurchaseEvent purchaseEvent = new PurchaseEvent(player, shopItem);
                            Bukkit.getServer().getPluginManager().callEvent(purchaseEvent);
                        } else {
                            MessageUtils.getInstance().sendMessageList(player, "NOT_ENOUGH_TOKENS", "%amount%", NumberFormat.getInstance().format(shopItem.getCost()));
                            soundUtils.playSound("NOT_ENOUGH_TOKENS");
                        }
                    }
                };
                ItemStack itemStack = shopItem.getDisplayItem();
                ItemMeta itemMeta = itemStack.getItemMeta();
                if (itemMeta.hasLore()) {
                    final List<String> lore = (List<String>) itemMeta.getLore();
                    for (int i = 0; i < lore.size(); ++i) {
                        String s = lore.get(i);
                        int price = shopItem.getCost();
                        s = s.replace("%price%", NumberFormat.getInstance().format(price));
                        lore.set(i, s);
                    }
                    itemMeta.setLore(lore);
                }
                itemStack.setItemMeta(itemMeta);
                menuItem.setItemStack(itemStack, true);
                menu.addMenuItem(menuItem, shopItem.getSlot());
            }
        } else {
            for (int slot : shop.getUpdateItemList().keySet()) {
                ShopItem shopItem = shop.getUpdateItemList().get(slot);
                MenuItem menuItem = new MenuItem() {
                    DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);

                    @Override
                    public void onClick(Player player, InventoryClickType clickType) {
                        if (dataPlayer == null) return;
                        if (dataPlayer.getBalance() >= shopItem.getCost()) {
                            menu.setBypassMenuCloseBehaviour(true);
                            dataPlayer.removeTokens(shopItem.getCost());
                            for (String command : shopItem.getCommandsList()) {
                                command = command.replaceAll("%player%", player.getName());
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                            }
                            menu.closeMenu(player);
                            soundUtils.playSound("SUCCESSFUL_PURCHASE");
                            MessageUtils.getInstance().sendMessageList(player, "PURCHASE_COMPLETE", "%item%", shopItem.getItemKey());
                            PurchaseEvent purchaseEvent = new PurchaseEvent(player, shopItem);
                            Bukkit.getServer().getPluginManager().callEvent(purchaseEvent);
                        } else {
                            MessageUtils.getInstance().sendMessageList(player, "NOT_ENOUGH_TOKENS", "%amount%", NumberFormat.getInstance().format(shopItem.getCost()));
                            soundUtils.playSound("NOT_ENOUGH_TOKENS");
                        }
                    }

                    @Override
                    public ItemStack getItemStack() {
                        ItemStack itemStack = shopItem.getDisplayItem();
                        List<String> newLore = new ArrayList<>();
                        for (String string : itemStack.getItemMeta().getLore()) {
                            string = string.replaceAll("%price%", NumberFormat.getInstance().format(shopItem.getCost()));
                            newLore.add(string);
                        }
                        ItemMeta itemMeta = itemStack.getItemMeta();
                        itemMeta.setLore(newLore);
                        itemStack.setItemMeta(itemMeta);
                        return itemStack;
                    }
                };
                menu.addMenuItem(menuItem, slot);
            }
        }
        menu.setMenuCloseBehaviour(new MenuAPI.MenuCloseBehaviour() {
            @Override
            public void onClose(Player player, Menu menu, boolean bypass) {
                if (!bypass) {
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            if (!menu.bypassMenuCloseBehaviour() && menu.getParent() != null) {
                                menu.getParent().openMenu(player);
                            }
                            soundUtils.playSound("CLOSE_CATEGORY");
                        }
                    }, 5L);
                }
            }
        });
        for (int free = 0; free < menu.getInventory().getSize(); ++free) {
            if (menu.getInventory().getItem(free) == null) {
                final MenuItem.UnclickableMenuItem item = (MenuItem.UnclickableMenuItem) new MenuItem.UnclickableMenuItem() {
                    @Override
                    public ItemStack getItemStack() {
                        return ItemUtils.getUnusedItem();
                    }
                };
                menu.addMenuItem(item, free);
            }
        }
        soundUtils.playSound("OPEN_CATEGORY");
        menu.openMenu(player);
        menu.scheduleUpdateTask(player, 20);
    }

}
