package com.primalgamesllc.tokens.data;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.database.Callback;
import com.primalgamesllc.tokens.database.Database;

import java.sql.ResultSet;
import java.util.UUID;

public class DataPlayer {

    private UUID uuid;
    private Integer balance;

    public DataPlayer(UUID uuid) {
        this.uuid = uuid;
        this.balance = 0;
    }

    public void setup() {
        Database database = Main.getInstance().getDataBase();
        try {
            database.executeQuery("SELECT * FROM users WHERE uuid = '" + this.uuid + "'", new Callback<ResultSet>() {
                @Override
                public void onSuccess(ResultSet success) {
                    try {
                        while (success.next()) {
                            int balance = success.getInt("balance");
                            setBalance(balance);
                        }
                    } catch (Exception e) {
                        onFailure((ResultSet) null);
                    }
                }

                @Override
                public void onFailure(ResultSet p0) {
                    System.out.print("[C0d3rTokens] Failed to execute query for " + getUuid());
                }
            });
        } catch (Exception e) {
            System.out.print("[C0d3rTokens] Failed to execute query for " + getUuid());
        }
    }

    public void cleanUp() {
        Database database = Main.getInstance().getDataBase();
        try {
            database.executeUpdate("DELETE FROM users WHERE uuid = '" + this.uuid + "'");
            database.executeUpdate("INSERT INTO users (uuid, balance) VALUES ('" + this.uuid + "', '" + this.balance + "')");
            System.out.print("[C0d3rTokens] Successfully executed query for " + getUuid());
        } catch (Exception e) {
            System.out.print("[C0d3rTokens] Failed to execute update for " + getUuid());
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public void giveTokens(int amount) {
        setBalance(balance + amount);
    }

    public void removeTokens(int amount) {
        setBalance(balance - amount);
    }
}