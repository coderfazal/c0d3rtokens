package com.primalgamesllc.tokens.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class DataManager implements Listener {

    private static DataManager instance;
    private CopyOnWriteArrayList<DataPlayer> players;

    public DataManager() {
        System.out.print("Loading DataManager to manage player's data.");
    }

    public static DataManager getInstance() {
        if (DataManager.instance == null) {
            synchronized (DataManager.class) {
                if (DataManager.instance == null) {
                    DataManager.instance = new DataManager();
                }
            }
        }
        return DataManager.instance;
    }

    public void register() {
        if (this.players != null) {
            this.players.clear();
        }
        this.players = new CopyOnWriteArrayList<DataPlayer>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            DataPlayer data = this.getByPlayer(p);
            if (data == null) {
                data = new DataPlayer(p.getUniqueId());
            }
            data.setup();
            this.players.add(data);
        }
    }

    public void unregister() {
        for (DataPlayer data : this.players) {
            data.cleanUp();
        }
        this.players.clear();
        this.players = null;
    }

    public DataPlayer getByPlayer(Player p) {
        for (DataPlayer player : this.players) {
            if (player.getUuid().equals(p.getUniqueId())) {
                return player;
            }
        }
        return null;
    }

    public DataPlayer getByUUID(UUID uuid) {
        for (DataPlayer player : this.players) {
            if (player.getUuid().equals(uuid)) {
                return player;
            }
        }
        return null;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        DataPlayer data = this.getByPlayer(e.getPlayer());
        if (data == null) {
            data = new DataPlayer(e.getPlayer().getUniqueId());
        }
        data.setup();
        this.players.add(data);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        DataPlayer data = this.getByPlayer(e.getPlayer());
        if (data != null) {
            data.cleanUp();
            this.players.remove(data);
        }
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        DataPlayer data = this.getByPlayer(e.getPlayer());
        if (data != null) {
            data.cleanUp();
            this.players.remove(data);
        }
    }

    public CopyOnWriteArrayList<DataPlayer> getPlayers() {
        return this.players;
    }

    public void setPlayers(CopyOnWriteArrayList<DataPlayer> players) {
        this.players = players;
    }

}