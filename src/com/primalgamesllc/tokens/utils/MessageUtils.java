package com.primalgamesllc.tokens.utils;

import com.primalgamesllc.tokens.Main;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageUtils {

    private static MessageUtils instance;

    public static MessageUtils getInstance() {
        if (MessageUtils.instance == null) {
            synchronized (MessageUtils.class) {
                if (MessageUtils.instance == null) {
                    MessageUtils.instance = new MessageUtils();
                }
            }
        }
        return MessageUtils.instance;
    }

    public void sendMessage(Player player, String configLocation) {
        player.sendMessage(Utils.toColor(Main.getInstance().getConfig().getString("messages." + configLocation)));
    }

    public void sendMessageList(Player player, String configLocation) {
        for (String message : Main.getInstance().getConfig().getStringList("messages." + configLocation)) {
            player.sendMessage(Utils.toColor(message));
        }
    }

    public void sendMessage(CommandSender player, String configLocation) {
        player.sendMessage(Utils.toColor(Main.getInstance().getConfig().getString("messages." + configLocation)));
    }

    public void sendMessageList(CommandSender player, String configLocation) {
        for (String message : Main.getInstance().getConfig().getStringList("messages." + configLocation)) {
            player.sendMessage(Utils.toColor(message));
        }
    }

    public void sendMessage(Player player, String configLocation, String replace, String replacement) {
        player.sendMessage(Utils.toColor(Main.getInstance().getConfig().getString("messages." + configLocation).replaceAll(replace, replacement)));
    }

    public void sendMessageList(Player player, String configLocation, String replace, String replacement) {
        for (String message : Main.getInstance().getConfig().getStringList("messages." + configLocation)) {
            message = message.replaceAll(replace, replacement);
            player.sendMessage(Utils.toColor(message));
        }
    }

    public void sendMessage(CommandSender player, String configLocation, String replace, String replacement) {
        player.sendMessage(Utils.toColor(Main.getInstance().getConfig().getString("messages." + configLocation).replaceAll(replace, replacement)));
    }

    public void sendMessageList(CommandSender player, String configLocation, String replace, String replacement) {
        for (String message : Main.getInstance().getConfig().getStringList("messages." + configLocation)) {
            message = message.replaceAll(replace, replacement);
            player.sendMessage(Utils.toColor(message));
        }
    }
}
