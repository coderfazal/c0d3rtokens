package com.primalgamesllc.tokens.utils;

import com.primalgamesllc.tokens.Main;
import com.primalgamesllc.tokens.objects.Shop;

import java.util.HashMap;
import java.util.Map;

public class ShopUtils {

    public static ShopUtils instance;

    public static ShopUtils getInstance() {
        if (ShopUtils.instance == null) {
            synchronized (ShopUtils.class) {
                if (ShopUtils.instance == null) {
                    ShopUtils.instance = new ShopUtils();
                }
            }
        }
        return instance;
    }

    public Map<Integer, Shop> getShops() {
        Map<Integer, Shop> shopList = new HashMap<Integer, Shop>();
        for (String shopKey : Main.getInstance().getConfig().getConfigurationSection("shops").getKeys(false)) {
            Shop shop = new Shop(shopKey);
            shopList.put(shop.getSlot(), shop);
        }
        return shopList;
    }

}
