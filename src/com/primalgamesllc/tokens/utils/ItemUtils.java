package com.primalgamesllc.tokens.utils;

import com.primalgamesllc.tokens.Main;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemUtils {

    public static ItemStack getUnusedItem() {
        return new ItemBuilder(Material.getMaterial(Main.getInstance().getConfig().getString("items.unused-item.material")), Main.getInstance().getConfig().getInt("items.unused-item.data")).setName(Main.getInstance().getConfig().getString("items.unused-item.name")).setLore(Main.getInstance().getConfig().getStringList("items.unused-item.lore")).getStack();
    }

    public static Map<String, ItemStack> getMobHeads() {
        Map<String, ItemStack> mobHeads = new HashMap<>();
        for (String mobDropsKey : Main.getInstance().getConfig().getConfigurationSection("mobDrops").getKeys(false)) {
            ConfigurationSection section = Main.getInstance().getConfig().getConfigurationSection("mobDrops." + mobDropsKey);
            String material = section.getString("displayItem.material");
            int data = section.getInt("displayItem.data");
            String name = section.getString("displayItem.name");
            List<String> lore = (List<String>) section.getStringList("displayItem.lore");
            ItemBuilder builder;
            if (material.equals("SKULL_ITEM") && data == 3) {
                builder = new ItemBuilder(Material.getMaterial(material), data).setName(name).setOwner(section.getString("displayItem.owner")).setLore(lore).setAmount(section.getInt("amount"));
                mobHeads.put(mobDropsKey, builder.getStack());

            } else {
                builder = new ItemBuilder(Material.getMaterial(material), data).setName(name).setLore(lore).setAmount(section.getInt("amount"));
                mobHeads.put(mobDropsKey, builder.getStack());
            }
        }
        return mobHeads;
    }

}
