package com.primalgamesllc.tokens.utils;

import com.primalgamesllc.tokens.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundUtils {

    private Player player;

    public SoundUtils(Player player) {
        this.player = player;
    }

    public void playSound(String configName) {
        if (Main.getInstance().getConfig().getBoolean("sounds." + configName + ".enabled")) {
            Sound sound = Sound.valueOf(Main.getInstance().getConfig().getString("sounds." + configName + ".sound"));
            if (sound == null) {
                sound = Sound.LEVEL_UP;
                System.out.print("[C0d3rTokens] The sound from config " + configName + " is not a valid sound therefore it's set to LEVEL_UP");
            }
            player.playSound(player.getLocation(), sound, 1.0F, 1.5F);
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
