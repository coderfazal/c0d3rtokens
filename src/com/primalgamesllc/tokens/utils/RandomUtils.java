package com.primalgamesllc.tokens.utils;

import java.util.Random;

public class RandomUtils {

    public static int getRandomInteger(int lower, int upper) {
        Random random = new Random();
        return random.nextInt((upper - lower) + 1) + lower;
    }

}
