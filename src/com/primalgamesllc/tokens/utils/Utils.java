package com.primalgamesllc.tokens.utils;

import org.bukkit.ChatColor;

import java.util.concurrent.TimeUnit;

public class Utils {

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    public static String toColor(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String formatTime(final Long timeLeft) {
        final int hours = (int) TimeUnit.MILLISECONDS.toHours(timeLeft);
        final int minute = (int) (TimeUnit.MILLISECONDS.toMinutes(timeLeft) - TimeUnit.HOURS.toMinutes(hours));
        final int second = (int) (TimeUnit.MILLISECONDS.toSeconds(timeLeft) - (TimeUnit.HOURS.toSeconds(hours) + TimeUnit.MINUTES.toSeconds(minute)));
        return String.format("%02d hours %02d minutes %02d seconds", hours, minute, second);
    }

}
