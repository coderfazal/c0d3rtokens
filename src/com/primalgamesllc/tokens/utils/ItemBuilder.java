package com.primalgamesllc.tokens.utils;

import com.google.common.collect.Lists;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ItemBuilder {
    private final ItemStack item;


    public ItemBuilder(Material material, int data) {
        if (material == null) {
            material = Material.STONE;
        }
        this.item = new ItemStack(material, 1, (short) ((byte) data));
    }

    public ItemBuilder glow() {
        this.item.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        this.item.getItemMeta().addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.item.setAmount(amount);
        return this;
    }

    public ItemBuilder setName(String name) {
        if (name == null) {
            name = "&7The name was found null";
        }
        ItemMeta meta = this.item.getItemMeta();
        meta.setDisplayName(Utils.toColor(name));
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        if (lore == null) {
            lore = new ArrayList<>();
            lore.add("&7Lore was found null.");
        }
        ItemMeta meta = this.item.getItemMeta();
        ArrayList<String> lores = Lists.newArrayList();
        Iterator var4 = lore.iterator();

        while (var4.hasNext()) {
            String s = (String) var4.next();
            lores.add(Utils.toColor(s));
        }

        meta.setLore(lores);
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        ItemMeta meta = this.item.getItemMeta();
        ArrayList<String> lores = Lists.newArrayList();
        String[] var4 = lore;
        int var5 = lore.length;

        for (int var6 = 0; var6 < var5; ++var6) {
            String s = var4[var6];
            lores.add(Utils.toColor(s));
        }

        meta.setLore(lores);
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment ench, int level) {
        this.item.addUnsafeEnchantment(ench, level);
        return this;
    }

    public ItemBuilder setColor(Color color) {
        if (!this.item.getType().toString().contains("LEATHER")) {
            throw new IllegalArgumentException("setColor can only be used on leather armour!");
        } else {
            LeatherArmorMeta meta = (LeatherArmorMeta) this.item.getItemMeta();
            meta.setColor(color);
            this.item.setItemMeta(meta);
            return this;
        }
    }

    public ItemBuilder setOwner(String name) {
        this.item.setType(Material.SKULL_ITEM);
        this.item.setDurability((short) ((byte) 3));
        SkullMeta meta = (SkullMeta) this.item.getItemMeta();
        meta.setOwner(name);
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemBuilder setDurability(int durability) {
        if (durability >= -32768 && durability <= 32767) {
            this.item.setDurability((short) durability);
            return this;
        } else {
            throw new IllegalArgumentException("The durability must be c short!");
        }
    }

    public ItemBuilder setData(MaterialData data) {
        ItemMeta meta = this.item.getItemMeta();
        this.item.setData(data);
        this.item.setItemMeta(meta);
        return this;
    }

    public ItemStack getStack() {
        return this.item;
    }
}

