package com.primalgamesllc.tokens.api;

import com.primalgamesllc.tokens.data.DataManager;
import com.primalgamesllc.tokens.data.DataPlayer;
import org.bukkit.entity.Player;

import java.text.NumberFormat;

public class C0d3rTokensAPI {

    private static C0d3rTokensAPI instance;

    public static C0d3rTokensAPI getInstance() {
        if (C0d3rTokensAPI.instance == null)
            synchronized (C0d3rTokensAPI.class) {
                if (C0d3rTokensAPI.instance == null) {
                    C0d3rTokensAPI.instance = new C0d3rTokensAPI();
                }
            }
        return null;
    }

    public int getTokens(Player player) {
        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
        if (dataPlayer != null) {
            return dataPlayer.getBalance();
        } else System.out.print("[C0d3rTokens] [API] The player provided was not found in the database.");
        return 0;
    }

    public String getTokensFormatted(Player player) {
        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
        if (dataPlayer != null) {
            return NumberFormat.getInstance().format(dataPlayer.getBalance());
        } else System.out.print("[C0d3rTokens] [API] The player provided was not found in the database.");
        return "0";
    }

    public void setTokens(Player player, int amount) {
        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
        if (dataPlayer != null) {
            dataPlayer.setBalance(amount);
        } else System.out.print("[C0d3rTokens] [API] The player provided was not found in the database.");
    }

    public void giveTokens(Player player, int amount) {
        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
        if (dataPlayer != null) {
            dataPlayer.giveTokens(amount);
        } else System.out.print("[C0d3rTokens] [API] The player provided was not found in the database.");
    }

    public void removeTokens(Player player, int amount) {
        DataPlayer dataPlayer = DataManager.getInstance().getByPlayer(player);
        if (dataPlayer != null) {
            dataPlayer.removeTokens(amount);
        } else System.out.print("[C0d3rTokens] [API] The player provided was not found in the database.");
    }

}
