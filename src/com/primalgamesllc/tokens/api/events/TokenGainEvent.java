package com.primalgamesllc.tokens.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TokenGainEvent extends Event {


    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private int oldBalance;
    private int newBalance;
    private int currentBalance;
    private boolean cancelled;

    public TokenGainEvent(Player player, int oldBal, int newBal) {
        this.player = player;
        this.oldBalance = oldBal;
        this.newBalance = newBal;
        this.currentBalance = newBal - oldBal;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public int getOldBalance() {
        return oldBalance;
    }

    public int getNewBalance() {
        return newBalance;
    }

    public int getCurrentBalance() {
        return currentBalance;
    }

    public Player getPlayer() {
        return player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}