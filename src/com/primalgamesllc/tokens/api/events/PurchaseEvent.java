package com.primalgamesllc.tokens.api.events;

import com.primalgamesllc.tokens.objects.ShopItem;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class PurchaseEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private ShopItem shopItem;

    public PurchaseEvent(Player player, ShopItem item) {
        this.player = player;
        this.shopItem = item;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

}
