package com.primalgamesllc.tokens.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Database {
    protected Connection conn;
    protected ExecutorService executor;
    private long lastValidation;

    public Database() {
        this.executor = Executors.newFixedThreadPool(1);
        this.lastValidation = System.currentTimeMillis();
    }

    public abstract void setup();

    public abstract void open();

    public void close() {
        try {
            if (this.conn == null || this.conn.isClosed()) {
                return;
            }
            this.conn.close();
            this.conn = null;
            this.executor = null;
        } catch (SQLException ignore) {
            ignore.printStackTrace();
        }
    }

    public synchronized Connection getConnection() {
        try {
            if (this.conn == null || this.conn.isClosed()) {
                this.open();
            }
            if (System.currentTimeMillis() - this.lastValidation >= 5000L) {
                this.executeQuery("SELECT 1", null);
                this.lastValidation = System.currentTimeMillis();
            }
        } catch (SQLException ignore) {
            this.open();
        }
        return this.conn;
    }

    public synchronized void executeUpdate(String s) {
        this.executor.submit(new Runnable() {
            @Override
            public void run() {
                PreparedStatement prepareStatement = null;
                try {
                    prepareStatement = Database.this.getConnection().prepareStatement(s);
                    prepareStatement.executeUpdate();
                } catch (SQLException ignore) {
                    ignore.printStackTrace();
                } finally {
                    try {
                        prepareStatement.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    prepareStatement.close();
                } catch (SQLException ex2) {
                }
            }
        });
    }

    public synchronized void executeQuery(String s, Callback<ResultSet> callback) {
        this.executor.submit(new Runnable() {
            @Override
            public void run() {
                PreparedStatement prepareStatement = null;
                ResultSet executeQuery = null;
                try {
                    prepareStatement = Database.this.getConnection().prepareStatement(s);
                    executeQuery = prepareStatement.executeQuery();
                    if (callback != null) {
                        callback.onSuccess(executeQuery);
                    }
                } catch (SQLException ignore) {
                    if (callback != null) {
                        callback.onFailure(null);
                    }
                    ignore.printStackTrace();
                } finally {
                    try {
                        executeQuery.close();
                        prepareStatement.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    executeQuery.close();
                    prepareStatement.close();
                } catch (SQLException ex2) {
                }
            }
        });
    }

    public synchronized void shutdown() {
        this.executor.submit(new Runnable() {
            @Override
            public void run() {
                Database.this.close();
                System.out.println("[C0d3rTokens] Closing connection to database and cleaning up data.");
            }
        });
        this.executor.shutdown();
    }
}

