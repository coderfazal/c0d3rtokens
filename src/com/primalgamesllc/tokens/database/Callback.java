package com.primalgamesllc.tokens.database;

public interface Callback<V> {
    void onSuccess(V p0);

    void onFailure(V p0);
}


