package com.primalgamesllc.tokens.database;

import com.google.common.io.Files;
import com.primalgamesllc.tokens.Main;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLiteDatabase extends Database {
    private File dbLocation;

    public SQLiteDatabase(File dbLocation) {
        this.dbLocation = dbLocation;
    }

    @Override
    public void setup() {
        this.executeUpdate("CREATE TABLE IF NOT EXISTS users (uuid VARCHAR(37), balance INTEGER)");
    }

    @Override
    public void open() {
        try {
            Files.createParentDirs(this.dbLocation);
            if (!this.dbLocation.exists()) {
                this.dbLocation.createNewFile();
            }
            Class.forName("org.sqlite.JDBC");
            (this.conn = DriverManager.getConnection("jdbc:sqlite:" + this.dbLocation.getAbsolutePath())).setAutoCommit(true);
        } catch (SQLException ignore2) {
            Main.getInstance().getLogger().severe("[C0d3rTokens] Plugin disabled due to SQLite error!");
            Bukkit.getPluginManager().disablePlugin((Plugin) Main.getInstance());
        } catch (ClassNotFoundException ignore3) {
            Main.getInstance().getLogger().severe("[C0d3rTokens] Plugin disabled due to no SQLite driver found!");
            Bukkit.getPluginManager().disablePlugin((Plugin) Main.getInstance());
        } catch (Exception ignore) {
            Main.getInstance().getLogger().severe("[C0d3rTokens] Plugin disabled due to an unknown error.");
            Bukkit.getPluginManager().disablePlugin((Plugin) Main.getInstance());
            ignore.printStackTrace();
        }
    }
}